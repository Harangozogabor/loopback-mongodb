const uuidV4 = require('uuid/v4');
var admin = require('firebase-admin');
var serviceAccount = require('../../server/pwatest-11735-firebase-adminsdk-ryz3x-35ab05f774.json');
module.exports = function (Person) {

  function authorization(options, calledFunction, callback) {
    if (options.accessToken && options.accessToken.firebaseToken) {
      admin.auth().verifyIdToken(options.accessToken.firebaseToken)
        .then((decodedToken) => {// exact user, authenticated user.
          var uid = decodedToken.uid;
          console.log(decodedToken.uid)
          Person.app.models.Person.find({ where: { id: uid } }, function (err, result) {
            if (result.length!=0 &&  result[0].role === 'admin') {
              calledFunction();
            } else {
              var error = new Error("Authorization error, missing role");
              error.status = 401;
              return callback(error);
            }
          });
        }).catch(function (error) {
          var error = new Error("Authorization error, bad token");
          error.status = 401;
          return callback(error);
        });
    } else {
      var error = new Error("Authorization error, missing token");
      error.status = 401;
      return callback(error);
    }
  }
  /**
   * addPerson
   * @param {Person} body undefined
   * @callback {Function} callback Callback function
   * @param {Error|string} err Error object
   * @param {any} result Result object
   */
  Person.addPerson = function (body, options, callback) {
    if (options.accessToken && options.accessToken.firebaseToken) {
      admin.auth().verifyIdToken(options.accessToken.firebaseToken)
        .then((decodedToken) => {// exact user, authenticated user.
          var uid = decodedToken.uid;
          var mongo = Person.app.models.Person
          body.role = 'user';
          body.createrUserId = uid;
          body.id=uid;
          mongo.create(body, function (err, result) {
            callback(err, result)
          });
        }).catch(function (error) {
          var error = new Error("Authorization error, bad token");
          error.status = 401;
          return callback(error);
        });
    } else {
      var error = new Error("Authorization error, missing token");
      error.status = 401;
      return callback(error);
    }


  }


  /**
   * getPerson
   * @param {string} id undefined
   * @param {string} email undefined
   * @callback {Function} callback Callback function
   * @param {Error|string} err Error object
   * @param {Person} result Result object
   */
  Person.getPerson = function (id, email, callback) {
    var mongo = Person.app.models.Person
    var body = {};
    id ? body.id = id : '';
    email ? body.email = email : '';
    console.log(body);
    mongo.find({
      where: body
    }, function (err, result) {
      callback(err, result)
    });

  }

  /**
 * isAdmin
 * @param {string} id undefined
 * @callback {Function} callback Callback function
 * @param {Error|string} err Error object
 * @param {Person} result Result object
 */
  Person.isAdmin = function (id, callback) {
    Person.app.models.Person.find({ where: { id: id } }, function (err, result) {
      if (result.length != 0 && result[0].role == 'admin') {
        callback(err, true)
      } else {
        callback(err, false)
      }
    });

  }

  /**
   * deletePerson
   * @param {string} id undefined
   * @callback {Function} callback Callback function
   * @param {Error|string} err Error object
   * @param {any} result Result object
   */
  Person.deletePerson = function (id, options, callback) {
    calledFunction = function () {
      var mongo = Person.app.models.Person
      mongo.remove({
        id: id
      }, function (err, result) {
        callback(err, result)
      });
    }
    authorization(options,calledFunction,callback)
  }


  /**
   * updatePerson
   * @param {string} id undefined
   * @param {string} firstName undefined
   * @param {string} lastName undefined
   * @param {string} email undefined
   * @param {string} phone undefined
   * @param {string} billingCity undefined
   * @param {string} billingPostCode undefined
   * @param {string} billingStreet undefined
   * @param {string} shippingCity undefined
   * @param {string} shippingPostCode undefined
   * @param {string} shippingStreet undefined
   * @param {string} companyName undefined
   * @param {string} companyTax undefined
   * @param {string} profileImage undefined
   * @callback {Function} callback Callback function
   * @param {Error|string} err Error object
   * @param {any} result Result object
   */
  Person.updatePerson = function (id, firstName, lastName, email, phone, billingCity, billingPostCode, billingStreet, shippingCity, shippingPostCode, shippingStreet, companyName, companyTax,profileImage, options, callback) {
    var mongo = Person.app.models.Person
    var body = {};
    body.id = id;
    body.firstName = firstName;
    body.lastName = lastName;
    body.email = email;
    body.phone = phone;
    body.billingCity = billingCity;
    body.billingPostCode = billingPostCode;
    body.billingStreet = billingStreet;
    body.shippingCity = shippingCity;
    body.shippingPostCode = shippingPostCode;
    body.shippingStreet = shippingStreet;
    body.companyName = companyName;
    body.companyTax = companyTax;
    body.profileImage=profileImage;
    console.log(options)
    if (options.accessToken && options.accessToken.firebaseToken) {
      admin.auth().verifyIdToken(options.accessToken.firebaseToken)
        .then((decodedToken) => {// exact user, authenticated user.
          var uid = decodedToken.uid;
          console.log(decodedToken.uid)
          Person.app.models.Person.find({ where: { id: uid } }, function (err, result) {
            console.log(result)
            if (result.length!=0 && result[0].role === 'admin') {
              console.log(body)
              mongo.update(
                { id: id },
                body
                , function (err, result) {
                  callback(err, result)
                });
            } else {
              Person.app.models.Person.find({ where: { id: id } }, function (err, result) {
                if (result.length!=0 && result[0].createrUserId === uid) {//creator and the requester are the same
                  console.log(body)
                  mongo.update(
                    { id: id },
                    body
                    , function (err, result) {
                      callback(err, result)
                    });
                } else {
                  var error = new Error("Authorization error, missing role");
                  error.status = 401;
                  return callback(error);
                }
              });
            }
          });
        }).catch(function (error) {
          var error = new Error("Authorization error, bad token");
          error.status = 401;
          return callback(error);
        });
    } else {
      var error = new Error("Authorization error, missing token");
      error.status = 401;
      return callback(error);
    }
  }



  Person.remoteMethod('addPerson',
    {
      isStatic: true,
      consumes: ['application/json'],
      accepts:
        [{
          arg: 'body',
          type: 'Person',
          description: undefined,
          required: undefined,
          http: { source: 'body' }
        }, { arg: 'options', type: 'object', http: 'optionsFromRequest' }],
      returns: [],
      http: { verb: 'post', path: '' },
      description: undefined
    }
  );

  Person.remoteMethod('isAdmin',
    {
      isStatic: true,
      consumes: ['application/json'],
      accepts:
        [{
          arg: 'id',
          type: 'string',
          description: undefined,
          required: undefined,
          http: { source: 'query' }
        }],
      returns: [{ description: 'OK', type: ['boolean'], arg: 'data', root: true }],
      http: { verb: 'get', path: '' },
      description: undefined
    }
  );

  Person.remoteMethod('getPerson',
    {
      isStatic: true,
      produces: ['application/json'],
      accepts:
        [{
          arg: 'id',
          type: 'string',
          description: undefined,
          required: false,
          http: { source: 'query' }
        },
        {
          arg: 'email',
          type: 'string',
          description: undefined,
          required: false,
          http: { source: 'query' }
        }],
      returns:
        [{ description: 'OK', type: ['Person'], arg: 'data', root: true }],
      http: { verb: 'get', path: '' },
      description: undefined
    }
  );

  Person.remoteMethod('deletePerson',
    {
      isStatic: true,
      consumes: ['application/json'],
      accepts:
        [{
          arg: 'id',
          type: 'string',
          description: undefined,
          required: true,
          http: { source: 'query' }
        }, { arg: 'options', type: 'object', http: 'optionsFromRequest' }],
      returns: [],
      http: { verb: 'delete', path: '' },
      description: undefined
    }
  );

  Person.remoteMethod('updatePerson',
    {
      isStatic: true,
      consumes: ['application/json'],
      accepts:
        [{
          arg: 'id',
          type: 'string',
          description: undefined,
          required: true,
          http: { source: 'path' }
        },
        {
          arg: 'firstName',
          type: 'string',
          description: undefined,
          required: true,
          http: { source: 'query' }
        },
        {
          arg: 'lastName',
          type: 'string',
          description: undefined,
          required: true,
          http: { source: 'query' }
        },
        {
          arg: 'email',
          type: 'string',
          description: undefined,
          required: true,
          http: { source: 'query' }
        },
        {
          arg: 'phone',
          type: 'string',
          description: undefined,
          required: true,
          http: { source: 'query' }
        },
        {
          arg: 'billingCity',
          type: 'string',
          description: undefined,
          required: true,
          http: { source: 'query' }
        },
        {
          arg: 'billingPostCode',
          type: 'string',
          description: undefined,
          required: true,
          http: { source: 'query' }
        },
        {
          arg: 'billingStreet',
          type: 'string',
          description: undefined,
          required: true,
          http: { source: 'query' }
        },
        {
          arg: 'shippingCity',
          type: 'string',
          description: undefined,
          required: true,
          http: { source: 'query' }
        },
        {
          arg: 'shippingPostCode',
          type: 'string',
          description: undefined,
          required: true,
          http: { source: 'query' }
        },
        {
          arg: 'shippingStreet',
          type: 'string',
          description: undefined,
          required: true,
          http: { source: 'query' }
        },
        {
          arg: 'companyName',
          type: 'string',
          description: undefined,
          required: true,
          http: { source: 'query' }
        },
        {
          arg: 'companyTax',
          type: 'string',
          description: undefined,
          required: true,
          http: { source: 'query' }
        },
        {
          arg: 'profileImage',
          type: 'string',
          description: undefined,
          required: true,
          http: { source: 'query' }
        }, { arg: 'options', type: 'object', http: 'optionsFromRequest' }
        ],
      returns: [],
      http: { verb: 'put', path: '/:id' },
      description: undefined
    }
  );
}
