

module.exports = function(Cart) {

/**
 * addCart
 * @param {Cart} body undefined
 * @callback {Function} callback Callback function
 * @param {Error|string} err Error object
 * @param {any} result Result object
 */
Cart.addCart= function(body, callback) {
    var mongo = Cart.app.models.Cart
    mongo.create(body,function(err, result) {
      callback(err,result)
    });
    
  }
  
  
  /**
   * getCart
   * @param {string} id undefined
   * @callback {Function} callback Callback function
   * @param {Error|string} err Error object
   * @param {Opinion} result Result object
   */
  Cart.getCart= function(id, callback) {
    var mongo = Cart.app.models.Cart
    var body = {};
    id ? body.id=id :'';
    console.log(body);
    mongo.find(
      {
        where:  body 
      }
    ,function(err, result) {
  
      callback(err,result)
    });
    
  }
  
  
  /**
   * deleteCart
   * @param {string} id undefined
   * @callback {Function} callback Callback function
   * @param {Error|string} err Error object
   * @param {any} result Result object
   */
  Cart.deleteCart = function(id, callback) {
    var mongo = Cart.app.models.Cart
    // Replace the code below with your implementation.
    // Please make sure the callback is invoked.
    mongo.remove({id: id
    },function(err, result) {
      callback(err,result)
    });
    
  }
  
  
  /**
   * updateCart
   * @param {string} id undefined
   * @param {array} products undefined
   * @callback {Function} callback Callback function
   * @param {Error|string} err Error object
   * @param {any} result Result object
   */
  Cart.updateCart = function(id,products, callback) {
    var mongo = Cart.app.models.Cart
    var body = {};
    products ? body.products=products :'';
    console.log(body);
  
    mongo.update(
      { id : id },
      body 
      ,function(err, result) {
      callback(err,result)
    });
    
    
  }
  
  
  
  

  Cart.remoteMethod('getCart',
    { isStatic: true,
    produces: [ 'application/json' ],
    accepts: 
     [ { arg: 'id',
         type: 'string',
         description: undefined,
         required: false,
         http: { source: 'query' } },
        ],
    returns: 
     [ { description: 'OK',
         type: [ 'Cart' ],
         arg: 'data',
         root: true } ],
    http: { verb: 'get', path: '' },
    description: undefined }
  );
  
  Cart.remoteMethod('deleteCart',
    { isStatic: true,
    consumes: [ 'application/json' ],
    accepts: 
     [ { arg: 'id',
         type: 'string',
         description: undefined,
         required: true,
         http: { source: 'query' } } ],
    returns: [],
    http: { verb: 'delete', path: '' },
    description: undefined }
  );

  Cart.remoteMethod('addCart',
  { isStatic: true,
  consumes: [ 'application/json' ],
  accepts: 
   [ { arg: 'body',
       type: 'Cart',
       description: undefined,
       required: undefined,
       http: { source: 'body' } } ],
  returns: [],
  http: { verb: 'post', path: '' },
  description: undefined }
);

  
  Cart.remoteMethod('updateCart',
    { isStatic: true,
    consumes: [ 'application/json' ],
    accepts: 
     [ { arg: 'id',
         type: 'string',
         description: undefined,
         required: true,
         http: { source: 'path' } },
       { arg: 'products',
         type: 'array',
         description: undefined,
         required: true,
         http: { source: 'body' } },
      ],
    returns: [],
    http: { verb: 'put', path: '/:id' },
    description: undefined }
  );
  
  }
  