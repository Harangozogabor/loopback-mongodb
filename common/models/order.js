const uuidV4 = require('uuid/v4');
var admin = require('firebase-admin');
var serviceAccount = require('../../server/pwatest-11735-firebase-adminsdk-ryz3x-35ab05f774.json');
const sgMail = require('@sendgrid/mail');
sgMail.setApiKey("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");
module.exports = function (Order) {

  function authorization(options, calledFunction, orderId, callback) {
    if (options.accessToken && options.accessToken.firebaseToken) {
      admin.auth().verifyIdToken(options.accessToken.firebaseToken)
        .then((decodedToken) => {// exact user, authenticated user.
          var uid = decodedToken.uid;
          console.log(decodedToken.uid)
          Order.app.models.Person.find({ where: { id: uid } }, function (err, result) {
            console.log(result)
            if (result ? result[0].role === 'admin':'') {
              calledFunction()
            } else {
              let body ={};
              orderId ? body.orderId=orderId : '';
              
              Order.app.models.Order.find({
                where: {
                  and: [
                    orderId ? body : '',
                    result[0].email ? { "user.email": result[0].email } : ''
                  ]
                }
              }, function (err, result) {
                if (result.length != 0 && result[0].createrUserId === uid) {//creator and the requester are the same
                  calledFunction()
                } else {
                  var error = new Error("Authorization error, missing role");
                  error.status = 401;
                  return callback(error);
                }
              });
            }
          });
        }).catch(function (error) {
          console.log(error)
          var error = new Error("Authorization error, bad token");
          error.status = 401;
          return callback(error);
        });
    } else {
      var error = new Error("Authorization error, missing token");
      error.status = 401;
      return callback(error);
    }
  }

  /**
   * addOrder
   * @param {Object} body undefined
   * @callback {Function} callback Callback function
   * @param {Error|string} err Error object
   * @param {any} result Result object
   */
  Order.addOrder = function (body, options, callback) {
    
    var mongo = Order.app.models.Order
    let products = [];
    let order = new Object();
    body.order_price_sum=0;
    for (let i = 0; i < body.products.length; i++) {
      Order.app.models.Product.getProduct(
       body.products[i].id, null, null,null, null, null, null, null, null, null
        , function (err, result) {
          console.log("------------------------------BODY----------------------------------------")
          console.log(result)
          body.order_price_sum+=body.products[i].amount*result[0].price;
          console.log(body.order_price_sum)
          result[0].amount = body.products[i].amount;
          products.push(result[0]);
          order.id= uuidV4();
          order.products = products;
          if(result[0].pictures[0]){
            order.products[0].picture=result[0].pictures[0];
          }else{
            order.products[0].picture="016320b0-5837-11e9-82db-418753decc6a.png";
          }
          order.user = {
            id: body.buyer.id ? body.buyer.id : uuidV4(),
            firstName: body.buyer.firstName,
            lastName: body.buyer.lastName,
            email: body.buyer.email,
            phone: body.buyer.phone,
            billingCity: body.buyer.billingCity,
            billingPostCode: body.buyer.billingPostCode,
            billingStreet: body.buyer.billingStreet,
            shippingCity: body.buyer.shippingCity,
            shippingPostCode: body.buyer.shippingPostCode,
            shippingStreet: body.buyer.shippingStreet,
            companyName: body.buyer.companyName,
            companyTax: body.buyer.companyTax
          }
          order.full_price=0;
          order.post_address = body.buyer.shippingPostCode + ' ' + body.buyer.shippingCity + ' ' + body.buyer.shippingStreet;
          order.billing_address = body.buyer.billingPostCode + ' ' + body.buyer.billingPostCode + ' ' + body.buyer.billingPostCode;
          order.status = 'Rendelés beérkezett'
          order.order_date = new Date().toISOString();
          order.expected_arrive_date = null;
          if (i == body.products.length - 1) {
            order.order_price_sum=body.order_price_sum;
            order.payment_type = body.paymentType;
            order.shipping_type = body.shippingType;
            switch (order.shipping_type) {
              case 'Normál szállítás a magyar Postával':
              order.shipping_price=1300;
                break;
              case 'Elsőbbségi szállítás a magyar Postával':
              order.shipping_price=2300;
                break;
            }
            order.full_price= order.shipping_price+order.order_price_sum;
            if (options.accessToken && options.accessToken.firebaseToken) {
              console.log(options.accessToken.firebaseToken)
              admin.auth().verifyIdToken(options.accessToken.firebaseToken)
                .then((decodedToken) => {// exact user, authenticated user.
                  var uid = decodedToken.uid;
                  order.createrUserId = uid;
                  console.log("------------------------------ORDER----------------------------------------")
                  console.log(order)
                  mongo.create(order, function (err, result) {
                    const msg = {
                      to: order.user.email,
                      from: "gabi.harangozo29@gmail.com",
                      subject: "Sikeres megrendelés!",
                      text: "Sikeres megrendelés!",
                      templateId: 'd-012464abf29c4897b677e70615bd796e',
                      dynamic_template_data: {
                        order:order,
                        environment: "https://ledlegloopback.herokuapp.com/api/Containers/container1/download/"
                      }
                    };
                    sgMail.send(msg).then(() => {
                      console.log(msg)
                      console.log("Sikeres email küldés")
                    }, error => {
                      console.log(error)
                    });
                    sgMail.send({
                      to: "gabi.harangozo29@gmail.com",
                      from: "gabi.harangozo29@gmail.com",
                      subject: "Új megrendelés érkezett!",
                      text: "Új megrendelés érkezett!",
                    }).then(() => {
                      console.log(msg)
                      console.log("Sikeres email küldés")
                    }, error => {
                      console.log(error)
                    });
                    callback(err, result)
                  });
                }).catch(function (error) {
                  var error = new Error("Authorization error, bad token");
                  error.status = 401;
                  return callback(error);
                });
            } else {
              order.createrUserId = '';
              mongo.create(order, function (err, result) {
                const msg = {
                  to:  order.user.email,
                  from: "gabi.harangozo29@gmail.com",
                  subject: "subject",
                  text: "text message",
                  templateId: 'd-012464abf29c4897b677e70615bd796e',
                  dynamic_template_data: {
                    order:order,
                    environment: "https://ledlegloopback.herokuapp.com/api/Containers/container1/download/"
                  }
                };
                sgMail.send(msg).then(() => {
                  console.log(msg)
                    console.log(order)
                  console.log("Sikeres email küldés")
                }, error => {
                  console.log(error)
                });
                callback(err, result)
              });
            }
          }
        });
    }


  }

  /**
   * getOrder
   * @param {string} id undefined
   * @param {string} status undefined
   * @param {string} amount undefined
   * @param {string} from_order_date undefined
   * @param {string} to_order_date undefined
   * @param {number} limit undefined
   * @param {number} email undefined
   * @callback {Function} callback Callback function
   * @param {Error|string} err Error object
   * @param {Order} result Result object
   */
  Order.getOrder = function (id, status, amount, from_order_date, to_order_date, limit, email, options, callback) {
    calledFunction = function () {
      var mongo = Order.app.models.Order
      var body = {};
      id ? body.id = id : '';
      status ? body.status = status : '';
      amount ? body.amount = amount : '';
      console.log(body);
      mongo.find(
        {
          limit: limit,
          where: {
            and: [
              from_order_date && to_order_date ? { order_date: { between: [from_order_date, to_order_date] } } : '',
              body,
              email ? { "user.email": email } : ''
            ]
          }
        }, function (err, result) {
          callback(err, result)
        });
    }
    authorization(options, calledFunction, id, callback)
  }


  /**
   * deleteOrder
   * @param {string} id undefined
   * @callback {Function} callback Callback function
   * @param {Error|string} err Error object
   * @param {any} result Result object
   */
  Order.deleteOrder = function (id, options, callback) {
    calledFunction = function () {
      var mongo = Order.app.models.Order
      // Replace the code below with your implementation.
      // Please make sure the callback is invoked.
      mongo.remove({
        id: id
      }, function (err, result) {
        callback(err, result)
      });
    }
    authorization(options, calledFunction, id, callback)
  }


  /**
   * updateOrder
   * @param {string} id undefined
   * @param {number} amount undefined
   * @param {string} status undefined
   * @param {string} post_address undefined
   * @param {string} full_price undefined
   * @param {string} expected_arrive_date undefined
   * @callback {Function} callback Callback function
   * @param {Error|string} err Error object
   * @param {any} result Result object
   */
  Order.updateOrder = function (id, amount, status, post_address, full_price, expected_arrive_date, options, callback) {
    calledFunction = function () {
      var mongo = Order.app.models.Order
      var body = {};
      amount ? body.amount = amount : '';
      status ? body.status = status : '';
      post_address ? body.post_address = post_address : '';
      full_price ? body.full_price = full_price : '';
      expected_arrive_date ? body.expected_arrive_date = expected_arrive_date : '';
      console.log(body);
      mongo.update(
        { id: id },
        body
        , function (err, result) {
          mongo.find({ where : {id:id}}, function (err, result){
            if(result){
              for(let i=0;i<result[0].products.length;i++){
                if(result[0].products[i].pictures[0]){
                  result[0].products[i].picture=result[0].products[i].pictures[0];
                }else{
                  result[0].products[i].picture="016320b0-5837-11e9-82db-418753decc6a.png";
                }
                
              }
              const msg = {
                to: result[0].user.email,
                from: "gabi.harangozo29@gmail.com",
                subject: "subject",
                text: "text message",
                templateId: 'd-8bc296389cf04e2eb294d58ab944432b',
                dynamic_template_data: {
                  order:result[0],
                  environment: "https://ledlegloopback.herokuapp.com/api/Containers/container1/download/"
                }
              };
              sgMail.send(msg).then(() => {
                console.log(msg)
                console.log("Sikeres email küldés")
              }, error => {
                console.log(error)
              });
            }
          })
          callback(err, result)
        });
    }
    authorization(options, calledFunction, id, callback)
  }




  Order.remoteMethod('addOrder',
    {
      isStatic: true,
      consumes: ['application/json'],
      accepts:
        [{
          arg: 'body',
          type: 'Order',
          description: undefined,
          required: undefined,
          http: { source: 'body' }
        }, { arg: 'options', type: 'object', http: 'optionsFromRequest' }],
      returns: [],
      http: { verb: 'post', path: '' },
      description: undefined
    }
  );

  Order.remoteMethod('getOrder',
    {
      isStatic: true,
      produces: ['application/json'],
      accepts:
        [{
          arg: 'id',
          type: 'string',
          description: undefined,
          required: false,
          http: { source: 'query' }
        },
        {
          arg: 'status',
          type: 'string',
          description: undefined,
          required: false,
          http: { source: 'query' }
        },
        {
          arg: 'amount',
          type: 'string',
          description: undefined,
          required: false,
          http: { source: 'query' }
        },
        {
          arg: 'from_order_date',
          type: 'string',
          description: undefined,
          required: false,
          http: { source: 'query' }
        },
        {
          arg: 'to_order_date',
          type: 'string',
          description: undefined,
          required: false,
          http: { source: 'query' }
        },
        {
          arg: 'limit',
          type: 'number',
          description: undefined,
          required: false,
          http: { source: 'query' }
        },
        {
          arg: 'email',
          type: 'string',
          description: undefined,
          required: false,
          http: { source: 'query' },

        }, { arg: 'options', type: 'object', http: 'optionsFromRequest' }],
      returns:
        [{ description: 'OK', type: ['Order'], arg: 'data', root: true }],
      http: { verb: 'get', path: '' },
      description: undefined
    }
  );

  Order.remoteMethod('deleteOrder',
    {
      isStatic: true,
      consumes: ['application/json'],
      accepts:
        [{
          arg: 'id',
          type: 'string',
          description: undefined,
          required: true,
          http: { source: 'query' }
        }, { arg: 'options', type: 'object', http: 'optionsFromRequest' }],
      returns: [],
      http: { verb: 'delete', path: '' },
      description: undefined
    }
  );

  Order.remoteMethod('updateOrder',
    {
      isStatic: true,
      consumes: ['application/json'],
      accepts:
        [{
          arg: 'id',
          type: 'string',
          description: undefined,
          required: true,
          http: { source: 'path' }
        },
        {
          arg: 'amount',
          type: 'number',
          description: undefined,
          required: false,
          http: { source: 'query' }
        },
        {
          arg: 'status',
          type: 'string',
          description: undefined,
          required: false,
          http: { source: 'query' }
        },
        {
          arg: 'post_address',
          type: 'string',
          description: undefined,
          required: false,
          http: { source: 'query' }
        },
        {
          arg: 'full_price',
          type: 'string',
          description: undefined,
          required: false,
          http: { source: 'query' }
        },
        {
          arg: 'expected_arrive_date',
          type: 'string',
          description: undefined,
          required: false,
          http: { source: 'query' }
        }, { arg: 'options', type: 'object', http: 'optionsFromRequest' }],
      returns: [],
      http: { verb: 'put', path: '/:id' },
      description: undefined
    }
  );

}
