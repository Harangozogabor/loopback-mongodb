const uuidV4 = require('uuid/v4');
var admin = require('firebase-admin');
var serviceAccount = require('../../server/pwatest-11735-firebase-adminsdk-ryz3x-35ab05f774.json');

module.exports = function (MainCategory) {



  /**
   * addMainCategory
   * @param {MainCategory} body undefined
   * @callback {Function} callback Callback function
   * @param {Error|string} err Error object
   * @param {any} result Result object
   */
  MainCategory.addMainCategory = function (body, options, callback) {
    if (options.accessToken && options.accessToken.firebaseToken) {
      admin.auth().verifyIdToken(options.accessToken.firebaseToken)
        .then((decodedToken) => {// exact user, authenticated user.
          var uid = decodedToken.uid;
          console.log(decodedToken.uid)
          MainCategory.app.models.Person.find({ where: { id: uid } }, function (err, result) {
            console.log(result)
            console.log(result)
            if (result ? result[0].role === 'admin':'') {
              body.id = uuidV4();
              body.createrUserId = uid;
              console.log(body);
              var mongo = MainCategory.app.models.MainCategory
              mongo.create(body, function (err, result) {
                callback(err, result)
              });
            } else {
              var error = new Error("Authorization error, missing role");
              error.status = 401;
              return callback(error);
            }
          });
        }).catch((error) => {
          console.log(error)
          var error = new Error("Authorization error, bad token");
          error.status = 401;
          return callback(error);
        });
    } else {
      var error = new Error("Authorization error, missing token");
      error.status = 401;
      return callback(error);
    }

  }


  /**
   * getMainCategory
   * @param {string} id undefined
   * @param {string} name undefined
   * @callback {Function} callback Callback function
   * @param {Error|string} err Error object
   * @param {MainCategory} result Result object
   */
  MainCategory.getMainCategory = function (id, name, callback) {
    
  var mongo = MainCategory.app.models.MainCategory
  var body = {};
  id ? body.id = id : '';
  name ? body.name = name : '';
  mongo.find({
    where: body
  }, function (err, result) {
    callback(err, result)
  });
}


/**
 * deleteMainCategory
 * @param {string} id undefined
 * @callback {Function} callback Callback function
 * @param {Error|string} err Error object
 * @param {any} result Result object
 */
MainCategory.deleteMainCategory = function (id, options, callback) {
  var mongo = MainCategory.app.models.MainCategory
  // Replace the code below with your implementation.
  // Please make sure the callback is invoked.

  calledFunction = function () {
    mongo.remove({
      id: id
    }, function (err, result) {
      callback(err, result)
    });
  }
  authorization(options, calledFunction, callback)

}


function authorization(options, calledFunction, callback) {
  if (options.accessToken && options.accessToken.firebaseToken) {
    admin.auth().verifyIdToken(options.accessToken.firebaseToken)
      .then((decodedToken) => {// exact user, authenticated user.
        var uid = decodedToken.uid;
        console.log(decodedToken.uid)
        MainCategory.app.models.Person.find({ where: { id: uid } }, function (err, result) {
          console.log(result)
          console.log(result[0].role)
          if (result[0].role === 'admin') {
            calledFunction();
          } else {
            var error = new Error("Authorization error, missing role");
            error.status = 401;
            return callback(error);
          }
        });
      }).catch(function (error) {
        var error = new Error("Authorization error, bad token");
        error.status = 401;
        return callback(error);
      });
  } else {
    var error = new Error("Authorization error, missing token");
    error.status = 401;
    return callback(error);
  }
}



MainCategory.remoteMethod('addMainCategory',
  {
    isStatic: true,
    consumes: ['application/json'],
    accepts:
      [{
        arg: 'body',
        type: 'MainCategory',
        description: undefined,
        required: undefined,
        http: { source: 'body' }
      }, { arg: 'options', type: 'object', http: 'optionsFromRequest' }],
    returns: [],
    http: { verb: 'post', path: '' },
    description: undefined
  }
);

MainCategory.remoteMethod('getMainCategory',
  {
    isStatic: true,
    produces: ['application/json'],
    accepts:
      [{
        arg: 'id',
        type: 'string',
        description: undefined,
        required: false,
        http: { source: 'query' }
      },
      {
        arg: 'name',
        type: 'string',
        description: undefined,
        required: false,
        http: { source: 'query' }
      }],
    returns:
      [{
        description: 'OK',
        type: ['MainCategory'],
        arg: 'data',
        root: true
      }],
    http: { verb: 'get', path: '' },
    description: undefined
  }
);

MainCategory.remoteMethod('deleteMainCategory',
  {
    isStatic: true,
    consumes: ['application/json'],
    accepts:
      [{
        arg: 'id',
        type: 'string',
        description: undefined,
        required: true,
        http: { source: 'query' }
      }, { arg: 'options', type: 'object', http: 'optionsFromRequest' }],
    returns: [],
    http: { verb: 'delete', path: '' },
    description: undefined
  }
);



}
