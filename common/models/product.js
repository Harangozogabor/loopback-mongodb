const uuidV4 = require('uuid/v4');
var admin = require('firebase-admin');
var serviceAccount = require('../../server/pwatest-11735-firebase-adminsdk-ryz3x-35ab05f774.json');
module.exports = function (Product) {

  function authorization(options, calledFunction, callback) {
    if (options.accessToken && options.accessToken.firebaseToken) {
      admin.auth().verifyIdToken(options.accessToken.firebaseToken)
        .then((decodedToken) => {// exact user, authenticated user.
          var uid = decodedToken.uid;
          console.log(decodedToken.uid)
          Product.app.models.Person.find({ where: { id: uid } }, function (err, result) {
            console.log(result)
            if (result ? result[0].role === 'admin':'') {
              calledFunction();
            } else {
              var error = new Error("Authorization error, missing role");
              error.status = 401;
              return callback(error);
            }
          });
        }).catch(function (error) {
          var error = new Error("Authorization error, bad token");
          error.status = 401;
          return callback(error);
        });
    } else {
      var error = new Error("Authorization error, missing token");
      error.status = 401;
      return callback(error);
    }
  }
  /**
   * addProduct
   * @param {Product} body undefined
   * @callback {Function} callback Callback function
   * @param {Error|string} err Error object
   * @param {any} result Result object
   */
  Product.addProduct = function (body, options, callback) {

    if (options.accessToken && options.accessToken.firebaseToken) {
      admin.auth().verifyIdToken(options.accessToken.firebaseToken)
        .then((decodedToken) => {// exact user, authenticated user.
          var uid = decodedToken.uid;
          console.log(decodedToken.uid)
          Product.app.models.Person.find({ where: { id: uid } }, function (err, result) {
            if (result.length!=0 && result[0].role === 'admin') {
              body.id = uuidV4();
              body.createrUserId = uid;
              var mongo = Product.app.models.Product
              mongo.create(body, function (err, result) {
                callback(err, result)
              });
            } else {
              var error = new Error("Authorization error, missing role");
              error.status = 401;
              return callback(error);
            }
          });
        }).catch(function (error) {
          var error = new Error("Authorization error, bad token");
          error.status = 401;
          return callback(error);
        });
    } else {
      var error = new Error("Authorization error, missing token");
      error.status = 401;
      return callback(error);
    }



  }

  /**
   * getProduct
   * @param {string} id undefined
   * @param {string} name undefined
   * @param {string} mainCategory_id undefined
   * @param {string} subCategory_id undefined
   * @param {number} from_price undefined
   * @param {number} to_price undefined
   * @param {number} available undefined
   * @param {string} dealer undefined
   * @param {number} limit undefined
   * @callback {Function} callback Callback function
   * @param {Error|string} err Error object
   * @param {Product} result Result object
   */
  Product.getProduct = function (id, name, mainCategory_id,subCategory_id, from_price, to_price, available, dealer, limit, options, callback) {
    console.log(options)
    var mongo = Product.app.models.Product
    var body = {};
    id ? body.id = id : '';
    available ? body.available = available : '';
    dealer ? body.dealer = dealer : '';
    mongo.find(
      {
        limit: limit,
        where: {
          and: [
            from_price && to_price ? { price: { between: [from_price, to_price] } } : '',
            body,
            mainCategory_id ? { "subCategory.mainCategory.id": mainCategory_id } : '',
            subCategory_id ? { "subCategory.id": subCategory_id } : '',
            name ? { name: { like: name } } : '',
          ]
        }
      }

      , function (err, result) {

        callback(err, result)
      });

  }


  /**
   * deleteProduct
   * @param {string} id undefined
   * @callback {Function} callback Callback function
   * @param {Error|string} err Error object
   * @param {any} result Result object
   */
  Product.deleteProduct = function (id, options, callback) {
    let calledFunction = function () {
      var mongo = Product.app.models.Product
      mongo.remove({
        id: id
      }, function (err, result) {
        callback(err, result)
      });
    }
    authorization(options, calledFunction, callback)
  }


  /**
   * updateProduct
   * @param {string} id undefined
   * @param {string} name undefined
   * @param {string} about undefined
   * @param {string} small_about undefined
   * @param {string} pictures undefined
   * @param {number} price undefined
   * @param {string} dealer undefined
   * @param {number} available undefined
   * @callback {Function} callback Callback function
   * @param {Error|string} err Error object
   * @param {any} result Result object
   */
  Product.updateProduct = function (id,name, about, small_about, pictures, price, dealer, available, options, callback) {
    let calledFunction = function () {
      var mongo = Product.app.models.Product
      var body = {};
      name ? body.name = name : '';
      about ? body.about = about : '';
      small_about ? body.small_about = small_about : '';
      pictures ? body.pictures = pictures : '';
      price ? body.price = price : '';
      available ? body.available = available : '';
      dealer ? body.dealer = dealer : '';
      console.log(body);

      mongo.update(
        { id: id },
        body
        , function (err, result) {
          callback(err, result)
        });
    }
    authorization(options, calledFunction, callback)
  }




  Product.remoteMethod('addProduct',
    {
      isStatic: true,
      consumes: ['application/json'],
      accepts:
        [{
          arg: 'body',
          type: 'Product',
          description: undefined,
          required: undefined,
          http: { source: 'body' }
        }, { arg: 'options', type: 'object', http: 'optionsFromRequest' }],
      returns: [],
      http: { verb: 'post', path: '' },
      description: undefined
    }
  );

  Product.remoteMethod('getProduct',
    {
      isStatic: true,
      produces: ['application/json'],
      accepts:
        [{
          arg: 'id',
          type: 'string',
          description: undefined,
          required: false,
          http: { source: 'query' }
        },
        {
          arg: 'name',
          type: 'string',
          description: undefined,
          required: false,
          http: { source: 'query' }
        },
        {
          arg: 'mainCategory_id',
          type: 'string',
          description: undefined,
          required: false,
          http: { source: 'query' }
        },
        {
          arg: 'subCategory_id',
          type: 'string',
          description: undefined,
          required: false,
          http: { source: 'query' }
        },
        {
          arg: 'from_price',
          type: 'string',
          description: undefined,
          required: false,
          http: { source: 'query' }
        },
        {
          arg: 'to_price',
          type: 'string',
          description: undefined,
          required: false,
          http: { source: 'query' }
        },
        {
          arg: 'available',
          type: 'number',
          description: undefined,
          required: false,
          http: { source: 'query' }
        },
        {
          arg: 'dealer',
          type: 'string',
          description: undefined,
          required: false,
          http: { source: 'query' }
        },
        {
          arg: 'limit',
          type: 'number',
          description: undefined,
          required: false,
          http: { source: 'query' }
        }, { arg: 'options', type: 'string', http: 'optionsFromRequest' }],
      returns:
        [{
          description: 'OK',
          type: ['Product'],
          arg: 'data',
          root: true
        }],
      http: { verb: 'get', path: '' },
      description: undefined
    }
  );

  Product.remoteMethod('deleteProduct',
    {
      isStatic: true,
      consumes: ['application/json'],
      accepts:
        [{
          arg: 'id',
          type: 'string',
          description: undefined,
          required: true,
          http: { source: 'query' }
        }, { arg: 'options', type: 'object', http: 'optionsFromRequest' }],
      returns: [],
      http: { verb: 'delete', path: '' },
      description: undefined
    }
  );

  Product.remoteMethod('updateProduct',
    {
      isStatic: true,
      consumes: ['application/json'],
      accepts:
        [{
          arg: 'id',
          type: 'string',
          description: undefined,
          required: true,
          http: { source: 'path' }
        },
        {
          arg: 'name',
          type: 'string',
          description: undefined,
          required: false,
          http: { source: 'query' }
        },
        {
          arg: 'about',
          type: 'string',
          description: undefined,
          required: false,
          http: { source: 'query' }
        },
        {
          arg: 'small_about',
          type: 'string',
          description: undefined,
          required: false,
          http: { source: 'query' }
        },
        {
          arg: 'pictures',
          type: 'string',
          description: undefined,
          required: false,
          http: { source: 'query' }
        },
        {
          arg: 'price',
          type: 'number',
          description: undefined,
          required: false,
          http: { source: 'query' }
        },
        {
          arg: 'dealer',
          type: 'string',
          description: undefined,
          required: false,
          http: { source: 'query' }
        },
        {
          arg: 'available',
          type: 'number',
          description: undefined,
          required: false,
          http: { source: 'query' }
        }, { arg: 'options', type: 'object', http: 'optionsFromRequest' }],
      returns: [],
      http: { verb: 'put', path: '/:id' },
      description: undefined
    }
  );

}
