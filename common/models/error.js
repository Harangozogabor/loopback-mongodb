'use strict';
const uuidV4 = require('uuid/v4');
module.exports = function(Errorhandler) {


  Errorhandler.addError = function (body, callback) {
        let mongo = Errorhandler.app.models.Error
        body.id = uuidV4();
        mongo.create(body, function (err, result) {
            callback(err,result);
        })
    };
    Errorhandler.getError = function (id, from_order_date, to_order_date, callback) {
        let mongo = Errorhandler.app.models.Error;
        let body = {};
        id ? body.id = id : '';
        mongo.find(
            {
              where: {
                and: [
                  from_order_date && to_order_date ? { order_date: { between: [from_order_date, to_order_date] } } : '',
                  body
                ]
              }
            }, function (err, result) {
              callback(err, result)
            });
    };
    Errorhandler.deleteErrors = function (id, callback) {
      let mongo = Errorhandler.app.models.Error;
      mongo.remove({
        id: id
      }, function (err, result) {
        callback(err, result)
      });
  };

  Errorhandler.remoteMethod('deleteErrors',
  {
    isStatic: true,
    consumes: ['application/json'],
    accepts:
      [{
        arg: 'id',
        type: 'string',
        description: undefined,
        required: false,
        http: { source: 'query' }
      }],
    returns: [],
    http: { verb: 'delete', path: '' },
    description: undefined
  }
);
    Errorhandler.remoteMethod('addError',
    {
      isStatic: true,
      consumes: ['application/json'],
      accepts:
        [{
          arg: 'body',
          type: 'Error',
          description: undefined,
          required: undefined,
          http: { source: 'body' }
        }],
      returns: [],
      http: { verb: 'post', path: '' },
      description: undefined
    }
  );
  Errorhandler.remoteMethod('getError',
  {
    isStatic: true,
    produces: ['application/json'],
    accepts:
      [{
        arg: 'id',
        type: 'string',
        description: undefined,
        required: false,
        http: { source: 'query' }
      },
      {
        arg: 'from_order_date',
        type: 'string',
        description: undefined,
        required: false,
        http: { source: 'query' }
      },
      {
        arg: 'to_order_date',
        type: 'string',
        description: undefined,
        required: false,
        http: { source: 'query' }
      }
     ],
    returns:
      [{
        description: 'OK',
        type: ['Error'],
        arg: 'data',
        root: true
      }],
    http: { verb: 'get', path: '' },
    description: undefined
  }
);
};
