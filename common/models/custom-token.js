/*jslint node:true*/
/*jslint nomen:true*/
/*jslint esversion:6*/
'use strict';

module.exports = function(Customtoken) {

	/** Define a custom token id creation process
	 */
	Customtoken.createCustomAccessTokenId = function (callback) {
		//console.log(Customtoken.app.models.CustomUser.getFirebaseToken())
		return callback(null,  Customtoken.app.models.CustomUser.getFirebaseToken());
	};

	/** Intercept token creation and substitute id with our custom id
	 */
	Customtoken.observe('before save', function(ctx, next) {
		if(ctx.instance.dontsave===true){
			return next();
		}else{
			return Customtoken.createCustomAccessTokenId(function(err, id) {
				ctx.instance.firebaseToken=id;
				if (err)
					return next(err);
				// Substitute id
				// Remember to use next() to not interrupt the flow of the call
				return next();
			});
		}
		
    });

    Customtoken.observe('access', function(ctx, next) {
		console.log("BEFORE ACCESS")
		console.log(ctx.query)
        ctx.query={ where: { firebaseToken: ctx.query.where.id },
        limit: 1,
        offset: 0,
        skip: 0 }
		return next();
    });
	Customtoken.observe('before save', function(ctx, next) {
		console.log("BEFORE SAVE")
		return next();
	});
    
};
