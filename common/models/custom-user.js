/*jslint node:true*/
/*jslint nomen:true*/
/*jslint esversion:6*/
'use strict';

module.exports = function (Customuser) {
    let firebaseToken;

	/** This call adds custom behaviour to the standard Loopback login.
	 *
	 *  Since it uses the User.login function of the User model, let's also
	 *  keep the same parameter structure.
	 */
    Customuser.addToLogin = function (credentials, include, callback) {
        //ÁLLÍTSUK BE A FIREBASE TOKENÉT
        // Invoke the default login function
        firebaseToken = credentials.firebaseToken ? credentials.firebaseToken : '';
        //console.log(firebaseToken)
        return Customuser.login(credentials, include, function (loginErr, loginToken) {
            if (loginErr)
                return callback(loginErr);
            // If needed, here we can use loginToken.userId to retrieve
            // the user from the datasource
            return callback(null, loginToken.toObject());
        });

    };

    

    Customuser.getFirebaseToken = function () {
        return firebaseToken;
    }

    Customuser.find_one = function (email, callback) {
        return Customuser.find({where: {email: email}}, function (err, user) {
            if (err)
                return callback(err);
            // If needed, here we can use loginToken.userId to retrieve
            // the user from the datasource

            // Here you can do something with the user info, or the token, or both

            // Return the access token
            return callback(null, user);

        });
    }

	/** Register a path for the new login function
	 */
    Customuser.remoteMethod('addToLogin', {
        'http': {
            'path': '/add_to_login',
            'verb': 'post'
        },
        'accepts': [
            {
                'arg': 'credentials',
                'type': 'object',
                'description': 'Login credentials',
                'required': true,
                'http': {
                    'source': 'body'
                }
            },
            {
                'arg': 'include',
                'type': 'string',
                'description': 'Related objects to include in the response. See the description of return value for more details.',
                'http': {
                    'source': 'query'
                }
            }
        ],
        'returns': [
            {
                'arg': 'token',
                'type': 'object',
                'root': true
            }
        ]
    });

    Customuser.remoteMethod('find_one', {
        'http': {
            'path': '/find_one',
            'verb': 'get'
        },
        'accepts': [
            {
                'arg': 'email',
                'type': 'string',
                'description': 'email',
                'required': false,
                'http': {
                    'source': 'query'
                }
            }
        ],
        'returns': [
            {
                'arg': 'user',
                'type': 'object',
                'root': true
            }
        ]
    });
};
