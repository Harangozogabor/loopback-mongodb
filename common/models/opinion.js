const uuidV4 = require('uuid/v4');
var admin = require('firebase-admin');
var serviceAccount = require('../../server/pwatest-11735-firebase-adminsdk-ryz3x-35ab05f774.json');
module.exports = function (Opinion) {

  function authorization(options, calledFunction, callback) {
    if (options.accessToken && options.accessToken.firebaseToken) {
      admin.auth().verifyIdToken(options.accessToken.firebaseToken)
        .then((decodedToken) => {// exact user, authenticated user.
          var uid = decodedToken.uid;
          console.log(decodedToken.uid)
          calledFunction();
        }).catch(function (error) {
          var error = new Error("Authorization error, bad token");
          error.status = 401;
          return callback(error);
        });
    } else {
      var error = new Error("Authorization error, missing token");
      error.status = 401;
      return callback(error);
    }
  }

  /**
   * addOpinion
   * @param {Opinion} body undefined
   * @callback {Function} callback Callback function
   * @param {Error|string} err Error object
   * @param {any} result Result object
   */
  Opinion.addOpinion = function (body, options, callback) {
    if (options.accessToken && options.accessToken.firebaseToken) {
      admin.auth().verifyIdToken(options.accessToken.firebaseToken)
        .then((decodedToken) => {// exact user, authenticated user.
          var uid = decodedToken.uid;
          body.id = uuidV4();
          body.createrUserId = uid;
          var mongo = Opinion.app.models.Opinion
          mongo.create(body, function (err, result) {
            callback(err, result)
          });
        }).catch(function (error) {
          var error = new Error("Authorization error, bad token");
          error.status = 401;
          return callback(error);
        });
    } else {
      var error = new Error("Authorization error, missing token");
      error.status = 401;
      return callback(error);
    }

  }


  /**
   * getOpinion
   * @param {string} id undefined
   * @param {number} rate undefined
   * @param {string} user_id undefined
   * @param {string} product_id undefined
   * @callback {Function} callback Callback function
   * @param {Error|string} err Error object
   * @param {Opinion} result Result object
   */
  Opinion.getOpinion = function (id, rate, user_id, product_id, callback) {
    var mongo = Opinion.app.models.Opinion
    var body = {};
    id ? body.id = id : '';
    rate ? body.rate = rate : '';
    console.log(body);
    mongo.find(
      {
        where: {
          and: [
            body,
            user_id ? { "user.id": user_id } : '',
            product_id ? { "product.id": product_id } : ''
          ]
        }
      }
      , function (err, result) {

        callback(err, result)
      });

  }


  /**
   * deleteOpinion
   * @param {string} id undefined
   * @callback {Function} callback Callback function
   * @param {Error|string} err Error object
   * @param {any} result Result object
   */
  Opinion.deleteOpinion = function (id, options, callback) {
    var mongo = Opinion.app.models.Opinion
    // Replace the code below with your implementation.
    // Please make sure the callback is invoked.

    if (options.accessToken && options.accessToken.firebaseToken) {
      admin.auth().verifyIdToken(options.accessToken.firebaseToken)
        .then((decodedToken) => {// exact user, authenticated user.
          var uid = decodedToken.uid;
          console.log(decodedToken.uid)
          MainCategory.app.models.Person.find({ where: { id: uid } }, function (err, result) {
            console.log(result)
            console.log(result[0].role)
            if (result[0].role === 'admin') {
              mongo.remove({
                id: id
              }, function (err, result) {
                callback(err, result)
              });
            } else {
              mongo.find({ where: { id: id } }, function (err, result) {
                if (result[0].createrUserId === uid) {//creator and the requester are the same
                  mongo.remove({
                    id: id
                  }, function (err, result) {
                    callback(err, result)
                  });
                } else {
                  var error = new Error("Authorization error, missing role");
                  error.status = 401;
                  return callback(error);
                }
              });
            }
          });
        }).catch(function (error) {
          var error = new Error("Authorization error, bad token");
          error.status = 401;
          return callback(error);
        });
    } else {
      var error = new Error("Authorization error, missing token");
      error.status = 401;
      return callback(error);
    }


  }


  /**
   * updateOpinion
   * @param {string} id undefined
   * @param {number} rate undefined
   * @param {string} about undefined
   * @callback {Function} callback Callback function
   * @param {Error|string} err Error object
   * @param {any} result Result object
   */
  Opinion.updateOpinion = function (id, rate, about, options, callback) {
    var mongo = Opinion.app.models.Opinion

    if (options.accessToken && options.accessToken.firebaseToken) {
      admin.auth().verifyIdToken(options.accessToken.firebaseToken)
        .then((decodedToken) => {// exact user, authenticated user.
          var uid = decodedToken.uid;
          console.log(decodedToken.uid)
          MainCategory.app.models.Person.find({ where: { id: uid } }, function (err, result) {
            console.log(result)
            console.log(result[0].role)
            if (result[0].role === 'admin') {
              var body = {};
              rate ? body.rate = rate : '';
              about ? body.about = about : '';
              console.log(body);
              mongo.update(
                { id: id },
                body
                , function (err, result) {
                  callback(err, result)
                });
            } else {
              mongo.find({ where: { id: id } }, function (err, result) {
                if (result[0].createrUserId === uid) {//creator and the requester are the same
                  var body = {};
                  rate ? body.rate = rate : '';
                  about ? body.about = about : '';
                  console.log(body);
                  mongo.update(
                    { id: id },
                    body
                    , function (err, result) {
                      callback(err, result)
                    });
                } else {
                  var error = new Error("Authorization error, missing role");
                  error.status = 401;
                  return callback(error);
                }
              });
            }
          });
        }).catch(function (error) {
          var error = new Error("Authorization error, bad token");
          error.status = 401;
          return callback(error);
        });
    } else {
      var error = new Error("Authorization error, missing token");
      error.status = 401;
      return callback(error);
    }



  }




  Opinion.remoteMethod('addOpinion',
    {
      isStatic: true,
      consumes: ['application/json'],
      accepts:
        [{
          arg: 'body',
          type: 'Opinion',
          description: undefined,
          required: undefined,
          http: { source: 'body' }
        }, { arg: 'options', type: 'object', http: 'optionsFromRequest' }],
      returns: [],
      http: { verb: 'post', path: '' },
      description: undefined
    }
  );

  Opinion.remoteMethod('getOpinion',
    {
      isStatic: true,
      produces: ['application/json'],
      accepts:
        [{
          arg: 'id',
          type: 'string',
          description: undefined,
          required: false,
          http: { source: 'query' }
        },
        {
          arg: 'rate',
          type: 'number',
          description: undefined,
          required: false,
          http: { source: 'query' }
        },
        {
          arg: 'user_id',
          type: 'string',
          description: undefined,
          required: false,
          http: { source: 'query' }
        },
        {
          arg: 'product_id',
          type: 'string',
          description: undefined,
          required: false,
          http: { source: 'query' }
        }],
      returns:
        [{
          description: 'OK',
          type: ['Opinion'],
          arg: 'data',
          root: true
        }],
      http: { verb: 'get', path: '' },
      description: undefined
    }
  );

  Opinion.remoteMethod('deleteOpinion',
    {
      isStatic: true,
      consumes: ['application/json'],
      accepts:
        [{
          arg: 'id',
          type: 'string',
          description: undefined,
          required: true,
          http: { source: 'query' }
        }, { arg: 'options', type: 'object', http: 'optionsFromRequest' }],
      returns: [],
      http: { verb: 'delete', path: '' },
      description: undefined
    }
  );

  Opinion.remoteMethod('updateOpinion',
    {
      isStatic: true,
      consumes: ['application/json'],
      accepts:
        [{
          arg: 'id',
          type: 'string',
          description: undefined,
          required: true,
          http: { source: 'path' }
        },
        {
          arg: 'rate',
          type: 'number',
          description: undefined,
          required: false,
          http: { source: 'query' }
        },
        {
          arg: 'about',
          type: 'string',
          description: undefined,
          required: false,
          http: { source: 'query' }
        }, { arg: 'options', type: 'object', http: 'optionsFromRequest' }],
      returns: [],
      http: { verb: 'put', path: '/:id' },
      description: undefined
    }
  );

}
