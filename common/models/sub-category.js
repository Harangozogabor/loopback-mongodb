const uuidV4 = require('uuid/v4');
var admin = require('firebase-admin');
var serviceAccount = require('../../server/pwatest-11735-firebase-adminsdk-ryz3x-35ab05f774.json');
module.exports = function (SubCategory) {

  function authorization(options, calledFunction, callback) {
    if (options.accessToken && options.accessToken.firebaseToken) {
      admin.auth().verifyIdToken(options.accessToken.firebaseToken)
        .then((decodedToken) => {// exact user, authenticated user.
          var uid = decodedToken.uid;
          console.log(decodedToken.uid)
          SubCategory.app.models.Person.find({ where: { id: uid } }, function (err, result) {
            console.log(result)
            if (result ? result[0].role === 'admin':'') {
              calledFunction();
            } else {
              var error = new Error("Authorization error, missing role");
              error.status = 401;
              return callback(error);
            }
          });
        }).catch(function (error) {
          var error = new Error("Authorization error, bad token");
          error.status = 401;
          return callback(error);
        });
    } else {
      var error = new Error("Authorization error, missing token");
      error.status = 401;
      return callback(error);
    }
  }
  /**
   * addSubCategory
   * @param {SubCategory} body undefined
   * @callback {Function} callback Callback function
   * @param {Error|string} err Error object
   * @param {any} result Result object
   */
  SubCategory.addSubCategory = function (body, options, callback) {
    if (options.accessToken && options.accessToken.firebaseToken) {
      admin.auth().verifyIdToken(options.accessToken.firebaseToken)
        .then((decodedToken) => {// exact user, authenticated user.
          var uid = decodedToken.uid;
          console.log(decodedToken.uid)
          SubCategory.app.models.Person.find({ where: { id: uid } }, function (err, result) {
            console.log(result)
            console.log(result[0].role)
            if (result.length!=0 && result[0].role === 'admin') {
              body.id = uuidV4();
              body.createrUserId = uid;
              var mongo = SubCategory.app.models.SubCategory
              mongo.create(body, function (err, result) {
                callback(err, result)
              });
            } else {
              var error = new Error("Authorization error, missing role");
              error.status = 401;
              return callback(error);
            }
          });
        }).catch(function (error) {
          var error = new Error("Authorization error, bad token");
          error.status = 401;
          return callback(error);
        });
    } else {
      var error = new Error("Authorization error, missing token");
      error.status = 401;
      return callback(error);
    }

  }


  /**
   * getSubCategory
   * @param {string} id undefined
   * @param {string} name undefined
   * @param {string} mainCategory_id undefined
   * @callback {Function} callback Callback function
   * @param {Error|string} err Error object
   * @param {SubCategory} result Result object
   */
  SubCategory.getSubCategory = function (id, name, mainCategory_id, callback) {
    var mongo = SubCategory.app.models.SubCategory

    var body = {};
    id ? body.id = id : '';
    name ? body.name = name : '';
    mongo.find(
      {
        where: {
          and: [
            body,
            mainCategory_id ? { "mainCategory.id": mainCategory_id } : ''
          ]
        }
      }
      , function (err, result) {

        callback(err, result)
      });
  }


  /**
   * deleteSubCategory
   * @param {string} id undefined
   * @callback {Function} callback Callback function
   * @param {Error|string} err Error object
   * @param {any} result Result object
   */
  SubCategory.deleteSubCategory = function (id, options, callback) {
    let calledFunction = function () {
      var mongo = SubCategory.app.models.SubCategory
      // Replace the code below with your implementation.
      // Please make sure the callback is invoked.

      mongo.remove({
        id: id
      }, function (err, result) {
        callback(err, result)
      });
    }
    authorization(options, calledFunction, callback)
  }





  SubCategory.remoteMethod('addSubCategory',
    {
      isStatic: true,
      consumes: ['application/json'],
      accepts:
        [{
          arg: 'body',
          type: 'SubCategory',
          description: undefined,
          required: undefined,
          http: { source: 'body' }
        }, { arg: 'options', type: 'object', http: 'optionsFromRequest' }],
      returns: [],
      http: { verb: 'post', path: '' },
      description: undefined
    }
  );

  SubCategory.remoteMethod('getSubCategory',
    {
      isStatic: true,
      produces: ['application/json'],
      accepts:
        [{
          arg: 'id',
          type: 'string',
          description: undefined,
          required: false,
          http: { source: 'query' }
        },
        {
          arg: 'name',
          type: 'string',
          description: undefined,
          required: false,
          http: { source: 'query' }
        },
        {
          arg: 'mainCategory_id',
          type: 'string',
          description: undefined,
          required: false,
          http: { source: 'query' }
        }],
      returns:
        [{
          description: 'OK',
          type: ['SubCategory'],
          arg: 'data',
          root: true
        }],
      http: { verb: 'get', path: '' },
      description: undefined
    }
  );

  SubCategory.remoteMethod('deleteSubCategory',
    {
      isStatic: true,
      consumes: ['application/json'],
      accepts:
        [{
          arg: 'id',
          type: 'string',
          description: undefined,
          required: true,
          http: { source: 'query' }
        }, { arg: 'options', type: 'object', http: 'optionsFromRequest' }],
      returns: [],
      http: { verb: 'delete', path: '' },
      description: undefined
    }
  );


}
