'use strict';

var loopback = require('loopback');
var boot = require('loopback-boot');
var app = module.exports = loopback();
var admin = require('firebase-admin');
var serviceAccount = require('./pwatest-11735-firebase-adminsdk-ryz3x-35ab05f774.json');
admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://ledleg-41cfc.firebaseio.com"
});

app.start = function() {
  app.use(loopback.token({
    model: app.models.CustomToken
  }));
  app.models.CustomUser.settings.acls = require('./user-acls.json');
  app.models.CustomToken.settings.acls = require('./token-acls.json');
  // start the web server
  return app.listen(function () {
    app.emit('started');
    var baseUrl = app.get('url').replace(/\/$/, '');
    console.log('Web server listening at: %s', baseUrl);
    if (app.get('loopback-component-explorer')) {
      var explorerPath = app.get('loopback-component-explorer').mountPath;
      console.log('Browse your REST API at %s%s', baseUrl, explorerPath);
    }
  });
};

// Bootstrap the application, configure models, datasources and middleware.
// Sub-apps like REST API are mounted via boot scripts.
boot(app, __dirname, function (err) {
  if (err) throw err;

  // start the server if `$ node server.js`
  if (require.main === module)
    app.start();
});
